import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    x = df[df['Name'].str.lower().str.contains('mr. ')]['Age'].isnull().sum()
    y = round(df[df['Name'].str.lower().str.contains('mr. ')]['Age'].median())
    k = df[df['Name'].str.lower().str.contains('mrs.')]['Age'].isnull().sum()
    m = round(df[df['Name'].str.lower().str.contains('mrs.')]['Age'].median())
    l = df[df['Name'].str.lower().str.contains('miss.')]['Age'].isnull().sum()
    n = round(df[df['Name'].str.lower().str.contains('miss.')]['Age'].median())
    return [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
